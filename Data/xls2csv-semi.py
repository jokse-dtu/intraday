#!/usr/local/bin/python3
#coding=utf-8

# Python script that converts .xls (actually .html) files into .csv file with semicolon seperator

from bs4 import BeautifulSoup
import sys
import re
import csv

def cell_text(cell):
    return " ".join(cell.stripped_strings)

soup = BeautifulSoup(sys.stdin.read(), 'html5lib')
output = csv.writer(sys.stdout, delimiter=';')

for table in soup.find_all('table'):
    for row in table.find_all('tr'):
        col = map(cell_text, row.find_all(re.compile('t[dh]')))
        output.writerow(col)
    output.writerow([])