# 1. Data

This folder contains the raw data, processed data, and the scripts used to processes the data.

## 1.1 Folder structure

The raw data is located in the directory called `_raw`. Each folder within this directory is a different source, e.g. the `nordpool` folder contains data from [Nordpool's FTP server](http://www.nordpoolspot.com/services/Power-Data-Services/Product-details/), while the `entso-e` folder contains data from [ENTSO-E transparency platform's FTP server](https://entsoe.zendesk.com/hc/en-us/articles/115000173266-Overview-of-data-download-options-on-Transparency-Platform). The subfolder structure is a replica of the folder structure on the FTP server. Data taken from a website is placed in a folder where the domain name is written in reverse, e.g. the folder `com.nordpoolspot` contains data from [nordpoolspot.com](http://www.nordpoolspot.com). The names of these subfolders corresponds to the title of the webpage (as best as possible, while still being intuitive). Data from other sources, such as Python packages, are placed in a folder with a corresponding name, e.g. the `workalendar` folder contains data from the [workalendar package](https://github.com/novafloss/workalendar).

__Example of the folder structure:__

    _raw/
        nordpool/
            Elbas/
            Elspot/
            Information/
            Operating_data/
            ...
        entso-e/
        com.holidayapi/
        com.nordpoolspot/
            wind power/
            wind power prognosis/
        com.nordpoolgroup.ummapi/
        workalendar/
        ...
    _altered/
       nordpool/
            Elbas/
            Elspot/
            Information/
            Operating_data/
            ...
        entso-e/
        com.holidayapi/
        com.nordpoolspot/
            wind power/
            wind power prognosis/
        com.nordpoolgroup.ummapi/
        workalendar/
        ...

## 1.2 Processing scripts and documentation

The scripts used to process the raw data as well as the data documentation are (mainly) contained in [Jupyter notebook](http://jupyter.org).

Here follows the references for the data, and the scripts/notebooks that have been used to process and clean the data.

### 1.2.1 Individual datasets

#### Elspot data

Data is manually downloaded from the [Nordpool website](https://www.nordpoolgroup.com/historical-market-data/).

We use the [elspot.ipynb](elspot.ipynb) script to process and clean the data.

#### Elbas data

Data is manually downloaded from the [Nordpool FTP server](https://www.nordpoolgroup.com/services/Power-Data-Services/Product-details/).

We use the [elbas.ipynb](elbas.ipynb) script to process and clean the data.

#### Forecast data

Data is manually downloaded from the [Nordpool FTP server](https://www.nordpoolgroup.com/services/Power-Data-Services/Product-details/).

We use the [forecast.ipynb](forecast.ipynb) script to process and clean the data.

Note: forecast [lead times](lead.ipynb) differs across country and type (wind, consumption and production).

Note: We also considered using [ENTSO-E forecast data](forecast-entsoe.ipynb). The forecasts in this dataset are spilt into on-shore wind, off-shore wind and PV. However, we decided against using it since data is only available from the end of 2014.

#### Holidays data

Data on holidays comes from the Python library [workalendar](https://github.com/novafloss/workalendar). No data is available for Lithuania.

We use the [holidays.ipynb](holidays.ipynb) script get the holidays data.

### 1.2.2 Merging individual datasets

We use the [merge.ipynb](merge.ipynb) script to merge the individual datasets into one.



