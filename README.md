Analysing whether underestimating, as apposed to overestimating, consumption or wind production results in a different sized price premium (i.e. difference between day-ahead price and the average intraday price).

[Analysis](Analysis/analysis.ipynb)  
[Data documentation and processing](Data/)  

-----

This is a follow-up on [Soyal et al. (2017)](http://dx.doi.org/10.1109/EEM.2017.7981920).